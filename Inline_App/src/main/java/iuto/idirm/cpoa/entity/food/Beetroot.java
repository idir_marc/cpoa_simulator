package iuto.idirm.cpoa.entity.food;

import iuto.idirm.cpoa.entity.Entity;
import iuto.idirm.cpoa.map.Biome;
import iuto.idirm.cpoa.map.Cardinals;

import java.util.Map;

/**
 * @since JDK 15
 */
public class Beetroot extends Food {
    public Beetroot(int id, int value) {
        super(id, value);
        this.meat = false;
    }

    @SuppressWarnings("DuplicatedCode")
    public static Beetroot make(int id, String[] args) {
        int value;
        try {
            value = Integer.parseInt(args[0]);
        } catch (NumberFormatException|ArrayIndexOutOfBoundsException e) {
            return null;
        }
        return new Beetroot(id, value);
    }

    public static int getDefaultFoodValue() {
        return 2;
    }

    @Override
    public String getDetails() {
        return String.format("Beetroot Entity\n  Id: %d\n  Location: %s\n  Energy: %d\n  meat: no",
                this.id, this.location, this.energy);
    }

    @Override
    public void move(Cardinals c) {

    }

    @Override
    public void grow() {

    }

    @Override
    public void eat(Food f) {

    }

    @Override
    public void attack(Entity e) {

    }

    @Override
    public void deathDrop() {

    }

    @Override
    public Map<Cardinals, Biome> look() {
        return null;
    }
}
