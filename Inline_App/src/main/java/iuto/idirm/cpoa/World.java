package iuto.idirm.cpoa;

import iuto.idirm.cpoa.entity.Entity;
import iuto.idirm.cpoa.entity.EntityManager;
import iuto.idirm.cpoa.map.Biome;
import iuto.idirm.cpoa.map.Coordinates;
import iuto.idirm.cpoa.map.Wind;
import iuto.idirm.cpoa.map.worldstate.DayNightState;
import iuto.idirm.cpoa.map.worldstate.WorldStates;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static iuto.idirm.cpoa.WorldFileManager.*;
import static iuto.idirm.cpoa.SimulationErrors.*;

/**
 * The class for a single simulation world.<br>
 * Instanced or accessed with {@link World#getInstance(int, int)}<br>
 * Can also be accessed with {@link World#getInstance()}<br>
 * <p>
 * Warning! While x and y indicate the world's X and Y size, the
 * coordinates actually start at 0 and end respectively at X - 1 and
 * Y - 1.
 * @author IDIR Marc
 * @version 0.0.1
 * @since JDK 15
 */
public final class World implements Serializable {
    private static World world;

    private final int x;
    private final int y;
    private int turn = 0;
    private DayNightState state;
    private final Wind wind = new Wind();
    private final Map<Coordinates, Biome> worldMap = new HashMap<>();

    private volatile boolean edit = false;

    private World(int x, int y) throws WorldDimensionsException {
        if (x < 1 || y < 1)
            throw new WorldDimensionsException("World dimensions cannot be negative or null");
        this.x = x;
        this.y = y;
        this.state = WorldStates.day;
        this.wind.init();
    }

    // init

    public static World getInstance(int x, int y) throws WorldDimensionsException {
        if (world == null) world = new World(x, y);
        return world;
    }

    public static World getInstance() {
        return world;
    }

    public void setBiome(int x, int y, Biome b) throws WorldDimensionsException {
        if (x < 1 || y < 1 || x > this.x || y > this.y)
            throw new WorldDimensionsException("Coordinates out of world boundaries");
        Coordinates c = new Coordinates(x-1, y-1);
        Integer[] ids = new Integer[]{};
        if (this.worldMap.get(c) != null) ids = this.worldMap.get(c).entitiesList();
        this.worldMap.put(c, b);
        for (int i : ids) this.worldMap.get(c).addEntity(i);
        this.edit = true;
    }

    public int addEntity(int x, int y, String[] args) throws WorldDimensionsException, EntityBuildingException {
        if (x < 1 || y < 1 || x > this.x || y > this.y)
            throw new WorldDimensionsException("Coordinates out of world boundaries");
        Coordinates c = new Coordinates(x-1, y-1);
        Entity e = EntityManager.make(args, c);
        // some space for eventual further manipulation
        int i = EntityManager.getInstance().addEntity(e);
        try {
            if (i != 0) this.worldMap.get(c).addEntity(i); // add entity to biome
        } catch (NullPointerException ex) {
            EntityManager.getInstance().removeEntity(i);
            throw ex;
        }
        this.edit = true;
        return i;
    }

    // load & save

    static void load(World w) {
        world = w;
    }

    public boolean isEdited() {
        return this.edit;
    }

    public void edit() {
        this.edit = true;
    }

    void save(String s) throws IOException {
        WorldFile wf = new WorldFileManager.WorldFile(s,s,this);
        WorldFileManager.save(wf);
        this.edit = false;
    }

    // getters

    public int getSizeX() {
        return x;
    }

    public int getSizeY() {
        return y;
    }

    public int getTurn() {
        return turn;
    }

    public Biome getBiomeAt(Coordinates c) {
        try {
            return this.worldMap.get(c);
        } catch (NullPointerException e) {
            return null;
        }
    }

    public Wind getWind() {
        return this.wind;
    }

    public boolean isGenerated() {
        return this.worldMap.size() == this.x * this.y;
    }

    public String map() {
        StringBuilder sb = new StringBuilder("  ");
        for (int j = 0; j < this.y; j++) {
            for (int i = 0; i < this.x; i++) {
                try {
                    sb.append(this.worldMap.get(new Coordinates(i, j)).toString());
                } catch (NullPointerException e) {
                    sb.append(Biome.defaultMapString());
                }
                sb.append(' ');
            }
            sb.append(AnsiCodes.ANSI_RESET);
            if (j+1 != this.y)
                sb.append("\n  ");
        }
        return sb.toString();
    }

    public String entityMapCount() {
        List<String> rows = new ArrayList<>();
        for (int i = 0; i < this.y; i++) rows.add("");
        for (int i = 0; i < this.x; i++) {
            int width = 0;
            List<String> col = new ArrayList<>();
            for (int j = 0; j < this.y; j++) {
                int e;
                Biome b = this.worldMap.get(new Coordinates(i,j));
                if (b == null) e = 0;
                else e = b.entityCount();
                String s = Integer.toString(e);
                if (s.length() > width) width = s.length();
                col.add(s);
            }
            for (int r = 0; r < this.y; r++) {
                String s = col.get(r);
                rows.set(r, String.format("%s%s%s  ",rows.get(r)," ".repeat(width-s.length()),s));
            }
        }
        return String.join("\n",rows);
    }

    // run

    public void run(int i) throws SimulationStartException {
        if (this.worldMap.size() < this.x * this.y) throw new SimulationStartException(
                String.format("Only %d out of %d biomes generated",this.worldMap.size(), x*y));
        for (int t = 0; t < i; t++) this.tick();
    }

    private void tick() {
        this.turn++;
        this.state.tick();
        this.wind.tick();
        List<Entity> entities = new ArrayList<>(EntityManager.getInstance().stream().toList());
        entities.sort(new Entity.EntityComparator());
        for (Entity e : entities) {
            if (e == null) continue;
            e.tick(); // lazy way
        }
    }

    public void updateState(DayNightState state) {
        this.state = state;
        this.edit = true;
    }

    public void removeEntity(Coordinates location, int entityId) {
        this.worldMap.get(location).remEntity(entityId);
        this.edit = true;
    }
}
