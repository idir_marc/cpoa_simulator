package iuto.idirm.cpoa.entity.food;

import iuto.idirm.cpoa.entity.Entity;
import iuto.idirm.cpoa.map.Biome;
import iuto.idirm.cpoa.map.Cardinals;

import java.util.Map;

/**
 * @since JDK 15
 */
public class RabbitMeat extends Food {
    public RabbitMeat(int id, int value) {
        super(id, value);
    }

    @SuppressWarnings("DuplicatedCode")
    public static RabbitMeat make(int id, String[] args) {
        int value;
        try {
            value = Integer.parseInt(args[0]);
        } catch (NumberFormatException|ArrayIndexOutOfBoundsException e) {
            return null;
        }
        return new RabbitMeat(id, value);
    }

    public static int getDefaultFoodValue() {
        return 4;
    }

    @Override
    public String getDetails() {
        return null;
    }

    @Override
    public void move(Cardinals c) {

    }

    @Override
    public void grow() {

    }

    @Override
    public void eat(Food f) {

    }

    @Override
    public void attack(Entity e) {

    }

    @Override
    public void deathDrop() {

    }

    @Override
    public Map<Cardinals, Biome> look() {
        return null;
    }
}
