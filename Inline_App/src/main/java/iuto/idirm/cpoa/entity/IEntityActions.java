package iuto.idirm.cpoa.entity;

import iuto.idirm.cpoa.entity.food.Food;
import iuto.idirm.cpoa.map.Biome;
import iuto.idirm.cpoa.map.Cardinals;

import java.util.Map;

/**
 * @since JDK 15
 */
public interface IEntityActions {
    void tick();
    void behaviour();
    void move(Cardinals c);
    void grow();
    void eat(Food f);
    void attack(Entity e);
    void deathDrop();
    Map<Cardinals, Biome> look();
}
