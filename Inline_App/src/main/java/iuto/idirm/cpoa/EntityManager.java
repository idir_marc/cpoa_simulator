package iuto.idirm.cpoa;

import iuto.idirm.cpoa.entities.Entity;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static iuto.idirm.cpoa.SimulationErrors.*;

public class EntityManager {
    private static EntityManager instance = new EntityManager();
    private final Map<UUID, Entity> entities = new HashMap<>();

    private EntityManager() {}

    public static EntityManager getInstance() {
        return instance;
    }

    static void load(EntityManager entityManager) {
        instance = entityManager;
    }

    public UUID addEntity(Entity entity) {
        if (this.entities.containsKey(entity.getId())) return null;
        this.entities.put(entity.getId(), entity);
        return entity.getId();
    }

    public Entity getEntity(UUID id) throws EntityNotFoundException {
        try {
            return this.entities.get(id);
        } catch (NullPointerException e) {
            throw new EntityNotFoundException(String.format("No entity with id %s",id.toString()));
        }
    }

    public String entityCounts() {
        if (this.entities.size() == 0) return "The current world is empty of any entity";
        Map<String, Integer> outMap = new HashMap<>();
        int k = 0;
        for (UUID id : this.entities.keySet()) {
            Entity e = this.entities.get(id);
            if (e == null) continue;
            k++;
            String eName = e.getEntityName();
            if (!outMap.containsKey(eName)) outMap.put(eName,0);
            outMap.put(eName,outMap.get(eName)+1);
        }
        if (k == 0) return "The current world is empty of any entity";
        StringBuilder out = new StringBuilder();
        int i = 0;
        for (String eName : outMap.keySet()) {
            i++;
            out.append(String.format("  %s : %d", eName, outMap.get(eName)));
            if (i != outMap.size()) out.append('\n');
        }
        return out.toString();
    }

    public String getEntityDetails(UUID id) throws EntityNotFoundException {
        try {
            return this.entities.get(id).getDetails();
        } catch (NullPointerException e) {
            throw new EntityNotFoundException(String.format("No entity with id %s",id.toString()));
        }
    }

    public boolean removeEntity(UUID id) {
        return (this.entities.remove(id) != null);
    }
}
