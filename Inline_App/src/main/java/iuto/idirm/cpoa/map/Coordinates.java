package iuto.idirm.cpoa.map;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Objects;

/**
 * @since JDK 15
 */
public record Coordinates(int x, int y) implements Serializable {

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) return true;
        if (!(o instanceof Coordinates c)) return false;
        return c.getX() == x && c.getY() == y;
    }

    @Override
    public String toString() {
        return String.format("%d:%d", x + 1, y + 1);
    }

    public static class CoordComparator implements Comparator<Coordinates> {
        @Override
        public int compare(Coordinates o1, Coordinates o2) {
            if (o1.getY() > o2.getY()) return 1;
            if (o1.getY() < o2.getY()) return -1;
            return Integer.compare(o1.getX(), o2.getX());
        }
    }
}
