package iuto.idirm.cpoa.entity.food;

import iuto.idirm.cpoa.entity.Entity;
import iuto.idirm.cpoa.map.Biome;
import iuto.idirm.cpoa.map.Cardinals;

import java.util.Map;

/**
 * @since JDK 15
 */
public class FoxMeat extends Food {
    public FoxMeat(int id, int value) {
        super(id, value);
    }

    @SuppressWarnings("DuplicatedCode")
    public static FoxMeat make(int id, String[] args) {
        int value;
        try {
            value = Integer.parseInt(args[0]);
        } catch (NumberFormatException|ArrayIndexOutOfBoundsException e) {
            return null;
        }
        return new FoxMeat(id, value);
    }

    public static int getDefaultFoodValue() {
        return 6;
    }

    @Override
    public String getDetails() {
        return null;
    }

    @Override
    public void move(Cardinals c) {

    }

    @Override
    public void grow() {

    }

    @Override
    public void eat(Food f) {

    }

    @Override
    public void attack(Entity e) {

    }

    @Override
    public void deathDrop() {

    }

    @Override
    public Map<Cardinals, Biome> look() {
        return null;
    }
}
