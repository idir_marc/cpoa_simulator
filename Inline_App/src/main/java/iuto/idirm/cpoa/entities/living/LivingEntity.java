package iuto.idirm.cpoa.entities.living;

import iuto.idirm.cpoa.entities.Entity;

public class LivingEntity extends Entity {
    @Override
    public boolean isLivingEntity() {
        return true;
    }

    @Override
    public String getEntityName() {
        return null;
    }

    @Override
    public String getDetails() {
        return null;
    }
}
