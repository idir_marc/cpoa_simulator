package iuto.idirm.cpoa.map.worldstate;

import iuto.idirm.cpoa.World;

/**
 * @since JDK 15
 */
public class DayState extends DayNightState {
    DayState() {
        this.age = 0;
        this.brightness = 2;
        this.duration = 3;
    }

    @Override
    public String typeString() {
        return "Day";
    }

    @Override
    public void endState() {
        this.age = 0;
        World.getInstance().updateState(WorldStates.evening);
    }
}
