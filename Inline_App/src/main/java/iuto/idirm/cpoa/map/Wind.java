package iuto.idirm.cpoa.map;

import java.io.Serializable;

import static iuto.idirm.cpoa.map.Cardinals.*;

/**
 * Wind class, used for simulating wind through a simulation world-map.<br>
 * It is considered for the wind to be constant through the whole map.<br>
 * <p>
 * This class is used to simulate the seeds' spreading and flight
 * difficulties for birds.<br>
 * It solely represents the wind direction (through two {@link Cardinals})
 * and the wind's angle between those two, along with the wind power.<br>
 * No further detail implemented.
 * @author IDIR Marc
 * @version 0.0.1
 * @since JDK 15
 */
public class Wind implements Serializable {
    private static final double pi = Math.PI;
    private Cardinals upDown;
    private double upDownStrength;
    private Cardinals rightLeft;
    private double rightLeftStrength;
    private double power;
    private double windAngle;

    public Wind() {
        this.upDown = UP;
        this.rightLeft = RIGHT;
        this.upDownStrength = 0;
        this.rightLeftStrength = 0;
        this.power = 1;
    }

    public void init() {
        this.windAngle = 2 * pi * Math.random();
        this.power = 0.5 + Math.random();
        this.update();
    }

    public void tick() {
        this.windAngle += 3 * Math.random() - 1.5;
        while (this.windAngle >= (2 * pi)) {
            this.windAngle -= (2 * pi);
        }
        while (this.windAngle <= 0) {
            this.windAngle += (2 * pi);
        }
        this.power += Math.min(Math.max(0, 0.1 * Math.random() - 0.05), 2);
        this.power = Math.min(2, Math.max(0, this.power + 0.5 * Math.random() - 0.25));
        this.update();
    }

    private void update() {
        double angle = this.windAngle;
        if (angle > 1.5 * pi) {
            this.upDown = DOWN;
            this.rightLeft = RIGHT;
            angle -= 1.5 * pi;
        } else if (angle > pi) {
            this.upDown = DOWN;
            this.rightLeft = LEFT;
            angle -= pi;
        } else if (angle > 0.5 * pi) {
            this.upDown = UP;
            this.rightLeft = LEFT;
            angle -= 0.5 * pi;
        } else {
            upDown = UP;
            rightLeft = RIGHT;
        }
        this.upDownStrength = Math.cos(angle);
        this.rightLeftStrength = Math.sin(angle);
    }

    public Cardinals getUpDown() {
        return upDown;
    }

    public Cardinals getRightLeft() {
        return rightLeft;
    }

    public double getUpDownStrength() {
        return upDownStrength;
    }

    public double getRightLeftStrength() {
        return rightLeftStrength;
    }

    public double getPower() {
        return power;
    }
}
