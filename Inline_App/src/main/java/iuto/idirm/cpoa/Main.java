package iuto.idirm.cpoa;

import iuto.idirm.cpoa.entity.EntityManager;
import iuto.idirm.cpoa.map.Biome;
import iuto.idirm.cpoa.map.aquatic.River;
import iuto.idirm.cpoa.map.aquatic.Sea;
import iuto.idirm.cpoa.map.ground.Desert;
import iuto.idirm.cpoa.map.ground.Forest;
import iuto.idirm.cpoa.map.ground.Meadow;
import iuto.idirm.cpoa.WorldFileManager.WorldFile;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.*;

import static iuto.idirm.cpoa.Command.*;
import static iuto.idirm.cpoa.SimulationErrors.*;

/**
 * @author IDIR Marc
 * @version 0.0.2
 * @since JDK 15
 */
public abstract class Main {
    public static final String version = "Alpha 0.2.2";
    private static final List<Command<Main>> commandsList = new ArrayList<>();
    private static final Map<String[],Runnable> commands = new HashMap<>();
    private static final Scanner scanner = new Scanner(System.in);
    private static final Set<WorldFile> worldSaves = new HashSet<>();

    public static void main(String[] args) throws NullCommandNameException, MethodClassException,
            NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        System.out.println("Welcome");
        init();
        app();
    }

    @SuppressWarnings("unchecked")
    private static void init() throws NullCommandNameException, NoSuchMethodException, MethodClassException {
        if (new File("saves/").mkdir()) System.out.println("Saves folder created");
        commandsList.add(new Command<Main>(null,"help", "h")
                .exec(Main.class.getDeclaredMethod("help", String[].class))
                .setDesc("Sends this message"));
        commandsList.add(new Command<Main>(null, "quit", "q", "exit")
                .exec(Main.class.getDeclaredMethod("end", String[].class))
                .setDesc("Ends the simulator"));
        commandsList.add(new Command<Main>(null, "makeworld", "newWorld", "mw")
                .exec(Main.class.getDeclaredMethod("makeWorld", String[].class))
                .setDesc("Creates a new simulation world"));
        commandsList.add(new Command<Main>(null,"setbiome", "sb")
                .exec(Main.class.getDeclaredMethod("setBiome", String[].class))
                .setDesc("Sets a biome to specific coordinates in the world"));
        commandsList.add(new Command<Main>(null, "fill", "fillbiome")
                .exec(Main.class.getDeclaredMethod("fill", String[].class))
                .setDesc("Fills a given area of the world with a specific biome"));
        commandsList.add(new Command<Main>(null, "worldmap", "map", "wm")
                .exec(Main.class.getDeclaredMethod("worldMap", String[].class))
                .setDesc("Shows the world's biomes map"));
        commandsList.add(new Command<Main>(null, "tick", "t", "tk", "tic")
                .exec(Main.class.getDeclaredMethod("tick", String[].class))
                .setDesc("Ticks the world simulation"));
        commandsList.add(new Command<Main>(null, "addentity", "add", "ae")
                .exec(Main.class.getDeclaredMethod("addEntity", String[].class))
                .setDesc("Adds an entity in the world"));
        commandsList.add(new Command<Main>(null, "entitiesmap", "entitymap", "emap")
                .exec(Main.class.getDeclaredMethod("entitiesMap", String[].class))
                .setDesc("Shows the world's entity counts map"));
        commandsList.add(new Command<Main>(null, "entitycount", "count")
                .exec(Main.class.getDeclaredMethod("entityCounts", String[].class))
                .setDesc("Shows the counts of all entity types present in the world, along with each' IDs list"));
        commandsList.add(new Command<Main>(null,"entitydetails", "ed", "entity")
                .exec(Main.class.getDeclaredMethod("entityDetails", String[].class))
                .setDesc("Shows details about the given entity"));
        commandsList.add(new Command<Main>(null, "saves", "listsaves", "saveslist")
                .exec(Main.class.getDeclaredMethod("listSaves", String[].class))
                .setDesc("Lists valid saves files from the saves folder"));
        commandsList.add(new Command<Main>(null,"save", "saveworld","worldsave")
                .exec(Main.class.getDeclaredMethod("saveWorld", String[].class))
                .setDesc("Saves the current world under the specified name"));
        commandsList.add(new Command<Main>(null, "load", "saveload", "loadsave", "loadworld")
                .exec(Main.class.getDeclaredMethod("loadWorld", String[].class))
                .setDesc("Loads the specified world"));
        commandsList.sort(new Command.CommandComparator());
    }

    private static void app() throws InvocationTargetException, IllegalAccessException {
        while (true) {
            System.out.print(" $ ");
            String command = scanner.nextLine();
            if (command.trim().length() == 0) continue;
            if (!execute(command.toLowerCase()))
                System.out.printf("Unknown command %s\nUse 'help' for more info",command);
            System.out.println();
        }
    }

    private static boolean execute(String s) throws InvocationTargetException, IllegalAccessException {
        for (Command<Main> c : commandsList) {
            if (c.run(s.trim().split(" "))) return true;
        }
        return false;
    }

    private static boolean confirm() {
        String in = scanner.nextLine();
        if (in.length() == 0) return false;
        return switch (in.toLowerCase()) {
            // change for a check in "affirmative array" with translations
            case "y", "yes", "1", "affirmative" -> true;
            default -> false;
        };
    }

    /* #############
    * - commands - *
    * ########### */

    static void tick(String[] args) {
        if (World.getInstance() == null) {
            System.out.print("World has not been instantiated");
        } else if (!World.getInstance().isGenerated()) {
            System.out.print("World is not fully generated yet.");
        } else {
            int i;
            try {
                i = Integer.parseInt(args[0]);
            } catch (NullPointerException|ArrayIndexOutOfBoundsException|NumberFormatException e) {
                i = 1;
            }
            try {
                World.getInstance().run(i);
                System.out.printf("Successfully ticked %d time%s",i,i>1?"s":"");
            } catch (SimulationStartException e) {
                System.out.print(e.getMessage());
            }
        }
    }

    // World edition

    static void worldMap(String[] args) {
        if (World.getInstance() == null) {
            System.out.print("World has not been instantiated");
            return;
        }
        System.out.print(World.getInstance().map());
    }

    static void entitiesMap(String[] args) {
        if (World.getInstance() == null) {
            System.out.print("World has not been instantiated");
            return;
        }
        System.out.print(World.getInstance().entityMapCount());
    }

    static void entityCounts(String[] args) {
        if (World.getInstance() == null) {
            System.out.print("World has not been instantiated");
            return;
        }
        System.out.print(EntityManager.getInstance().entityCounts());
    }

    static void addEntity(String[] args) {
        if (World.getInstance() == null) {
            System.out.print("World has not been instantiated");
            return;
        }
        try {
            String[] s = args[0].split(":");
            int x = Integer.parseInt(s[0]);
            int y = Integer.parseInt(s[1]);
            int i = World.getInstance().addEntity(x,y, Arrays.copyOfRange(args,1, args.length));
            if (i == 0) System.out.print("An error occurred while registering the entity. Please repeat.");
            else System.out.printf("Successfully added entity with ID %d to the world", i);
        } catch (WorldDimensionsException e) {
            System.out.print("Dimensions out of world boundaries");
        } catch (EntityBuildingException e) {
            System.out.print(e.getMessage());
        } catch (NullPointerException e) {
            System.out.print("No biome set on this position");
        } catch (NumberFormatException|ArrayIndexOutOfBoundsException e) {
            System.out.print("Error: invalid arguments");
        }
    }

    static void entityDetails(String[] args) {
        try {
            System.out.print(EntityManager.getInstance().getEntityDetails(Integer.parseInt(args[0])));
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.print("Not enough arguments");
        } catch (NumberFormatException e) {
            System.out.print("Invalid argument");
        }
    }

    static void fill(String[] args) {
        if (World.getInstance() == null) {
            System.out.print("World has not been instantiated");
            return;
        }
        if (args.length < 3) {
            System.out.print("Not enough arguments");
            return;
        }
        try {
            String[] pos1 = args[0].split(":");
            String[] pos2 = args[1].split(":");
            int x1 = Integer.parseInt(pos1[0]);
            int y1 = Integer.parseInt(pos1[1]);
            int x2 = Integer.parseInt(pos2[0]);
            int y2 = Integer.parseInt(pos2[1]);
            if (x2 < x1) {
                int t = x2;
                x2 = x1;
                x1 = t;
            }
            if (y2 < y1) {
                int t = y2;
                y2 = y1;
                y1 = t;
            }
            int y = y1;
            Class<? extends Biome> b = switch (args[2].toLowerCase()) {
                case "river" -> River.class;
                case "sea" -> Sea.class;
                case "meadow" -> Meadow.class;
                case "forest" -> Forest.class;
                case "desert" -> Desert.class;
                default -> null;
            };
            if (b == null) {
                System.out.printf("No such biome: %s", args[2]);
                return;
            }
            while (y <= y2) {
                for (int k = x1; k <= x2; k++) {
                    //noinspection deprecation
                    World.getInstance().setBiome(k, y, b.newInstance());
                }
                y ++;
            }
            System.out.printf(
                    "Successfully filled space between %d:%d and %d:%d with %s biome",
                    x1, y1, x2, y2,b.getSimpleName());
        } catch (NumberFormatException|ArrayIndexOutOfBoundsException|InstantiationException|IllegalAccessException e) {
            System.out.printf("Error parsing arguments '%s'%n",String.join(" ",Arrays.asList(args)));
        } catch (SimulationErrors.WorldDimensionsException e) {
            System.out.print(e.getMessage());
        }
    }

    static void setBiome(String[] args) {
        if (World.getInstance() == null) {
            System.out.print("World has not been instantiated");
            return;
        }
        if (args.length < 2) {
            System.out.print("Not enough arguments");
            return;
        }
        try {
            String[] location = args[0].split(":");
            int x = Integer.parseInt(location[0]);
            int y = Integer.parseInt(location[1]);
            Biome b = null;
            switch (args[1].toLowerCase()) {
                case "river" -> b = new River();
                case "sea" -> b = new Sea();
                case "meadow" -> b = new Meadow();
                case "forest" -> b = new Forest();
                case "desert" -> b = new Desert();
                default -> System.out.printf("Unknown biome '%s'%n",args[1]);
            }
            if (b == null) System.out.printf("No such biome: %s",args[1]);
            else {
                World.getInstance().setBiome(x, y, b);
                System.out.printf("Successfully set %s biome at %d:%d",b.getClass().getSimpleName(), x, y);
            }
        } catch (NumberFormatException|ArrayIndexOutOfBoundsException e) {
            System.out.printf("Error parsing arguments '%s'%n",String.join(" ",Arrays.asList(args)));
        } catch (SimulationErrors.WorldDimensionsException e) {
            System.out.print(e.getMessage());
        }
    }

    static void makeWorld(String[] args) {
        if (World.getInstance() != null) {
            System.out.print("World already generated");
            return;
        }
        String dims;
        if (args.length == 0) {
            System.out.print("Enter new world dimensions (<height>X<width>) ");
            dims = scanner.nextLine().trim();
        } else dims = args[0];
        try {
            if (dims.length() == 0 || dims.equals("c")) {
                System.out.print("Cancelled.");
                return;
            }
            String[] d = dims.split("[x]");
            World.getInstance(Integer.parseInt(d[0]), Integer.parseInt(d[1]));
            System.out.printf("World successfully generated with dimensions %sx%s",d[0],d[1]);
        } catch (IllegalStateException|NullPointerException|IndexOutOfBoundsException e) {
            System.out.print("Could not find matching pattern <height>X<width>");
        } catch (SimulationErrors.WorldDimensionsException|NumberFormatException e) {
            System.out.printf("Error making world\n%s",e.getMessage());
        }
    }

    // World save files

    static void listSaves(String[] args) {
        List<WorldFile> files = new ArrayList<>(WorldFileManager.saveFiles());
        for (int i = 0; i < files.size(); i++) if (files.get(i) == null) {
            files.remove(i);
            i--;
        }
        worldSaves.clear();
        worldSaves.addAll(files);
        files.forEach(f -> System.out.printf("   - %s\n",f));
        System.out.printf("Accounting a total of %d save%s", files.size(), files.size() == 1 ? "" : "s");
    }

    static void saveWorld(String[] args) {
        if (World.getInstance() == null) {
            System.out.print("World has not been generated yet");
            return;
        }
        if (args.length == 0) {
            System.out.print("Not enough arguments");
            return;
        }
        try {
            World.getInstance().save(args[0]);
            WorldFile wf = new WorldFile(args[0], args[0], World.getInstance());
            worldSaves.remove(wf);
            worldSaves.add(wf);
            System.out.printf("Successfully saved the current world as %s.cws",args[0]);
        } catch (IOException e) {
            System.out.printf("Could not save current world as %s\nPlease check file access rights",args[0]);
        }
    }

    static void loadWorld(String[] args) {
        if (args.length == 0) {
            System.out.print("Not enough arguments.");
            return;
        }
        boolean b;
        if (World.getInstance() == null || !World.getInstance().isEdited()) b = true;
        else {
            if (args.length > 1 && args[1].equals("y")) b = true;
            else {
                System.out.print("There are some unsaved changes. Do you still want to load the save file? [y/n] ");
                b = confirm();
            }
        }
        if (!b) {
            System.out.print("Save file loading cancelled.");
            return;
        }
        String name = args[0];
        for (WorldFile wf : worldSaves) {
            if (wf.worldName.equals(name)) {
                if (!wf.compatible()) {
                    System.out.print("Incompatible world version");
                    return;
                }
                try {
                    WorldFileManager.load(wf);
                    System.out.print("World successfully loaded.");
                } catch (IOException | ClassNotFoundException e) {
                    System.out.print("Unable to load world from the file.");
                }
                return;
            }
        }
        System.out.print("No world with the specified name.");
    }

    // Genuine commands

    static void end(String[] args) {
        try {
            if (args[0].equals("y")) {
                System.out.println("Bye!");
                System.exit(0);
            }
        } catch (NullPointerException|ArrayIndexOutOfBoundsException ignored) {}
        System.out.print("Quit app? (warning, all non-saved changes will be lost) [y/n] ");
        if (confirm()) {
            System.out.println("Bye!");
            System.exit(0);
        } else System.out.print("Cancelled.");
    }

    static void help(String[] args) {
        System.out.print("Commands list: \n");
        for (int i = 0; i < commandsList.size(); i++) {
            Command<Main> command = commandsList.get(i);
            String s = String.format("  %s [%s] - %s",
                    command.getName(), String.join("/",command.getAliases()), command.getDesc());
            if (i+1 != commandsList.size()) s += "\n";
            System.out.print(s);
        }
    }
}
