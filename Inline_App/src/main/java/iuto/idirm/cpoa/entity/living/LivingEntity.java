package iuto.idirm.cpoa.entity.living;

import iuto.idirm.cpoa.World;
import iuto.idirm.cpoa.entity.Entity;
import iuto.idirm.cpoa.entity.EntityActions;
import iuto.idirm.cpoa.map.Cardinals;
import iuto.idirm.cpoa.map.Coordinates;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @since JDK 15
 */
public abstract class LivingEntity extends Entity {
    protected Map<EntityActions, Integer> actionsPriority = new HashMap<>(); // structure décisionnelle non implémentée
    protected List<LivingEntity> family = new ArrayList<>();

    public LivingEntity(int id) {
        super(id);
        this.gender = Math.random() >= 0.5;
    }

    protected abstract int[] reproduce(LivingEntity e);

    protected final void pushMove(Cardinals c) {
        this.actionsPriority.put(EntityActions.MOVE, 10);
    }

    @Override
    public void move(Cardinals c) {
        World.getInstance().getBiomeAt(this.location).remEntity(this.id);
        if (c == Cardinals.RIGHT || c == Cardinals.LEFT)
            this.location = new Coordinates(this.location.getX()+c.factor,this.location.getY());
        else this.location = new Coordinates(this.location.getX(), this.location.getY()+c.factor);
        World.getInstance().getBiomeAt(this.location).addEntity(this.id);
        this.energy -= this.weight+ World.getInstance().getBiomeAt(this.location).getEnergyExpense();
        if (this.age.ageLevel() > 1) this.family.forEach(e -> e.pushMove(c));
    }
}
