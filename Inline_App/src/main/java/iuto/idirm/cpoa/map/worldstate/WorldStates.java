package iuto.idirm.cpoa.map.worldstate;

/**
 * @since JDK 15
 */
public abstract class WorldStates {
    public static final DayState day = new DayState();
    public static final EveningState evening = new EveningState();
    public static final NightState night = new NightState();
    public static final MorningState morning = new MorningState();
}
