package iuto.idirm.cpoa.entities.item;

import iuto.idirm.cpoa.entities.Entity;

public class ItemEntity extends Entity {
    @Override
    public boolean isItemEntity() {
        return true;
    }

    @Override
    public String getEntityName() {
        return null;
    }

    @Override
    public String getDetails() {
        return null;
    }
}
