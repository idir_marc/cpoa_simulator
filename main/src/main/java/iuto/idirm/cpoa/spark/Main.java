package iuto.idirm.cpoa.spark;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import org.apache.velocity.app.Velocity;
import spark.Spark;

public abstract class Main {
    private static int port;

    public static void main(String[] args) {
        try {
            port = Integer.parseInt(args[0]);
        } catch (NumberFormatException|IndexOutOfBoundsException e) {
            port = 1234;
        }
        Spark.port(port);

        Spark.staticFileLocation("/static");
        //Views.loadRoutes();
        Velocity.setProperty("TEMPLATE_ROOT","models");
    }

    private static void openLocal() {
        String url = String.format("http://localhost:%d",port);

        if(Desktop.isDesktopSupported()){
            Desktop desktop = Desktop.getDesktop();
            try {
                desktop.browse(new URI(url));
            } catch (IOException | URISyntaxException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }else{
            Runtime runtime = Runtime.getRuntime();
            try {
                runtime.exec("xdg-open " + url);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }
}
