#!/bin/bash

echo -ne "\033]0;Java Populations Simulator\007"

whereis javac | grep javac -q;

if [ $? == 1 ] ; then
  echo "Could not find javac";
  exit 1;
fi

javac --version | grep 15 -q;

if [ $? == 1 ] ; then
  javac --version | grep 16 -q;
  
  if [ $? == 1 ] ; then
    javac --version | grep 17 -q;
    
    if [ $? == 1 ] ; then
    
      echo "A 15+ version of Java is required. Change your default or install a compatible one.";
      exit 1;
    
    fi;
  fi;
fi;

javac iuto/idirm/cpoa/Main.java -d out/

cd out;

java iuto.idirm.cpoa.Main
