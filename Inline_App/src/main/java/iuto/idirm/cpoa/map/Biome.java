package iuto.idirm.cpoa.map;

import iuto.idirm.cpoa.AnsiCodes;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * @since JDK 15
 */
public abstract class Biome implements Serializable {
    protected float seeThrough;
    protected int humidity;
    protected int energyExpense;
    protected final Set<Integer> localEntities = new HashSet<>();

    public final boolean addEntity(int id) {
        return this.localEntities.add(id);
    }

    public final boolean remEntity(int id) {
        return this.localEntities.remove(id);
    }

    public final int entityCount() {
        return this.localEntities.size();
    }

    public final Integer[] entitiesList() {
        return this.localEntities.toArray(new Integer[]{});
    }

    public final int getEnergyExpense() {
        return this.energyExpense;
    }

    public static String defaultMapString() {
        return String.format("%sO", AnsiCodes.ANSI_RESET);
    }

    @Override
    public String toString() {
        return String.format("%sO", AnsiCodes.ANSI_RESET);
    }
}
