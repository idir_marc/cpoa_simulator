package iuto.idirm.cpoa.entity.state.age;

import iuto.idirm.cpoa.entity.Entity;
import iuto.idirm.cpoa.entity.food.Food;
import iuto.idirm.cpoa.map.Biome;
import iuto.idirm.cpoa.map.Cardinals;

import java.util.Map;

/**
 * @since JDK 15
 */
public class AdultState extends EntityAgeState {
    public AdultState(Entity e) {
        super(e);
    }

    @Override
    public void tick() {
        this.since++;
    }

    @Override
    public void behaviour() {
        // Map<Cardinals, Biome> surroundings = this.look(); // for

    }

    @Override
    public int ageLevel() {
        return 2;
    }

    @Override
    public void move(Cardinals c) {

    }

    @Override
    public void grow() {
        if (this.since > 3*this.entity.getGrowthSpeed()) this.entity.switchAgeState(new ElderState(this.entity));
    }

    @Override
    public void eat(Food f) {

    }

    @Override
    public void attack(Entity e) {

    }

    @Override
    public void deathDrop() {

    }

    @Override
    public int deathFoodQuality() {
        return 3;
    }

    @Override
    public Map<Cardinals, Biome> look() {
        return this.entity.look();
    }

    @Override
    public String toString() {
        return "adult";
    }
}
