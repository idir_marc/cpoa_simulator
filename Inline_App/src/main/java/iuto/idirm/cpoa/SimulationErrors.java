package iuto.idirm.cpoa;

/**
 * @since JDK 15
 */
public abstract class SimulationErrors {
    public static class SimulationStartException extends Exception {
        public SimulationStartException(String s) {
            super(s);
        }
    }
    public static class WorldDimensionsException extends Exception {
        public WorldDimensionsException(String s) {
            super(s);
        }
    }
    public static class EntityBuildingException extends Exception {
        public EntityBuildingException(String s) {
            super(s);
        }
    }
    public static class EntityNotFoundException extends Exception {
        public EntityNotFoundException(String s) {
            super(s);
        }
    }
}
