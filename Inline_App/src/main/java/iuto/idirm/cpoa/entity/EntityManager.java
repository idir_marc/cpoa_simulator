package iuto.idirm.cpoa.entity;

import iuto.idirm.cpoa.SimulationErrors;
import iuto.idirm.cpoa.entity.food.Beetroot;
import iuto.idirm.cpoa.entity.food.Carrot;
import iuto.idirm.cpoa.entity.food.FoxMeat;
import iuto.idirm.cpoa.entity.food.RabbitMeat;
import iuto.idirm.cpoa.entity.living.Cat;
import iuto.idirm.cpoa.entity.living.Fox;
import iuto.idirm.cpoa.entity.living.Mouse;
import iuto.idirm.cpoa.entity.living.Rabbit;
import iuto.idirm.cpoa.entity.vegetal.BeetrootPlant;
import iuto.idirm.cpoa.entity.vegetal.CarrotPlant;
import iuto.idirm.cpoa.map.Coordinates;

import java.io.Serializable;
import java.util.*;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * @since JDK 15
 */
public class EntityManager implements Iterable<Entity>, Serializable {
    private static final EntityManager instance = new EntityManager();
    private final Map<Integer,Entity> entities = new HashMap<>();

    private EntityManager() {}

    public static EntityManager getInstance() {
        return instance;
    }

    public int addEntity(Entity newEntity) {
        if (this.entities.containsKey(newEntity.getId())) return 0;
        this.entities.put(newEntity.getId(), newEntity);
        return newEntity.getId();
    }

    public int entityNewID() {
        return this.entities.size()+1;
    }

    public Entity getEntity(int id) throws SimulationErrors.EntityNotFoundException {
        try {
            return this.entities.get(id);
        } catch (NullPointerException e) {
            throw new SimulationErrors.EntityNotFoundException(String.format("No entity with ID %d",id));
        }
    }

    public String entityCounts() {
        if (this.entities.size() == 0) return "No entity are present in the world";
        Map<String, List<Integer>> outMap = new HashMap<>();
        int k = 0;
        for (int i : this.entities.keySet()) {
            Entity e = this.entities.get(i);
            if (e == null) continue;
            k++;
            String eName = this.entities.get(i).getClass().getSimpleName();
            if (!outMap.containsKey(eName)) outMap.put(eName, new ArrayList<>());
            outMap.get(eName).add(i);
        }
        if (k == 0) return "There is currently no entity in the world.";
        StringBuilder out = new StringBuilder();
        int i = 0;
        for (String key : outMap.keySet()) {
            i++;
            out.append(String.format("  %s : %d - %s",key, outMap.get(key).size(), outMap.get(key)));
            if (i != outMap.size()) out.append('\n');
        }
        return out.toString();
    }

    public String getEntityDetails(int i) {
        try {
            return this.entities.get(i).getDetails();
        } catch (NullPointerException e) {
            return "No such entity";
        }
    }

    public boolean removeEntity(int id) {
        try {
            this.entities.put(id,null);
            return true;
        } catch (NullPointerException e) {
            return false;
        }
    }

    public static Entity make(String[] args, Coordinates location) throws SimulationErrors.EntityBuildingException {
        if (args == null || args.length == 0) return null;
        int newId = instance.entityNewID();
        Entity e = switch (args[0].toLowerCase()) {
            // Food
            case "carrot" -> Carrot.make(newId, Arrays.copyOfRange(args,1,args.length));
            case "beetroot" -> Beetroot.make(newId, Arrays.copyOfRange(args, 1, args.length));
            case "rabbitmeat" -> RabbitMeat.make(newId, Arrays.copyOfRange(args, 1, args.length));
            case "foxmeat" -> FoxMeat.make(newId, Arrays.copyOfRange(args, 1, args.length));
            // LivingEntities
            case "cat" -> Cat.make(newId, Arrays.copyOfRange(args, 1, args.length));
            case "fox" -> Fox.make(newId, Arrays.copyOfRange(args, 1, args.length));
            case "mouse" -> Mouse.make(newId, Arrays.copyOfRange(args, 1, args.length));
            case "rabbit" -> Rabbit.make(newId, Arrays.copyOfRange(args, 1, args.length));
            // VegetalEntities
            case "carrotplant" -> CarrotPlant.make(newId, Arrays.copyOfRange(args, 1, args.length));
            case "beetrootplant" -> BeetrootPlant.make(newId, Arrays.copyOfRange(args, 1, args.length));
            // default
            default -> null;
        };
        if (e == null) throw new SimulationErrors.EntityBuildingException("Invalid arguments. Could not build entity");
        e.setLocation(location);
        return e;
    }

    @Override
    public Iterator<Entity> iterator() {
        return entities.values().iterator();
    }

    public Stream<Entity> stream() {
        return StreamSupport.stream(spliterator(), false);
    }
}
