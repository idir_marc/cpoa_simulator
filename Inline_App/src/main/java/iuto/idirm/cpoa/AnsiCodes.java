package iuto.idirm.cpoa;

/**
 * Different ANSI color/style codes<br>
 * Sorted by types (fg/bg),
 * intensity (HI = 'High Intensity')
 * and color.
 * @since JDK 15
 */
public enum AnsiCodes {

    ANSI_RESET                 ("\u001B[0m" ),
    ANSI_REVERSED              ("\u001B[7;Nm"),

    ANSI_FOREGROUND_BLACK      ("\u001B[30m"),
    ANSI_FOREGROUND_RED        ("\u001B[31m"),
    ANSI_FOREGROUND_GREEN      ("\u001B[32m"),
    ANSI_FOREGROUND_YELLOW     ("\u001B[33m"),
    ANSI_FOREGROUND_BLUE       ("\u001B[34m"),
    ANSI_FOREGROUND_MAGENTA    ("\u001B[35m"),
    ANSI_FOREGROUND_CYAN       ("\u001B[36m"),
    ANSI_FOREGROUND_WHITE      ("\u001B[37m"),

    ANSI_FOREGROUND_HI_BLACK   ("\u001B[0;90m"),
    ANSI_FOREGROUND_HI_RED     ("\u001B[0;91m"),
    ANSI_FOREGROUND_HI_GREEN   ("\u001B[0;92m"),
    ANSI_FOREGROUND_HI_YELLOW  ("\u001B[0;93m"),
    ANSI_FOREGROUND_HI_BLUE    ("\u001B[0;94m"),
    ANSI_FOREGROUND_HI_MAGENTA ("\u001B[0;95m"),
    ANSI_FOREGROUND_HI_CYAN    ("\u001B[0;96m"),
    ANSI_FOREGROUND_HI_WHITE   ("\u001B[0;97m");


    private final String code;


    AnsiCodes(String s) {
        this.code = s;
    }

    @Override
    public String toString() {
        return this.code;
    }
}
