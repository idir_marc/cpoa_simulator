package iuto.idirm.cpoa.map.worldstate;

import java.io.Serializable;

/**
 * @since JDK 15
 */
public abstract class DayNightState implements Serializable {
    protected int brightness;
    protected int duration;
    protected int age;

    public abstract String typeString();
    public abstract void endState();

    public final float getBrightness() {
        return this.brightness;
    }

    public final void tick() {
        this.age ++;
        if (this.age == this.duration) {
            this.endState();
        }
    }
}
