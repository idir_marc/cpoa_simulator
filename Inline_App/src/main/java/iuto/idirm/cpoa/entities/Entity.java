package iuto.idirm.cpoa.entities;

import iuto.idirm.cpoa.World;
import iuto.idirm.cpoa.entity.state.age.EntityAgeState;
import iuto.idirm.cpoa.map.Coordinates;

import java.io.Serializable;
import java.util.UUID;

public abstract class Entity implements Serializable {
    private final UUID id = UUID.randomUUID();
    protected int age;
    protected EntityAgeState ageState;
    protected int energy;
    protected int weight;
    protected int health;
    protected int growthFactor;
    protected boolean gender;
    protected Coordinates location;
    protected int speed;
    protected int lastTickTurn = World.getInstance().getTurn();

    public boolean isLivingEntity() {
        return false;
    }

    public boolean isItemEntity() {
        return false;
    }

    public boolean isOther() {
        return false;
    }

    public final UUID getId() {
        return id;
    }

    public final EntityAgeState getAgeState() {
        return this.ageState;
    }

    public final int getGrowthFactor() {
        return this.growthFactor;
    }

    public final void switchAgeState(EntityAgeState state) {
        this.ageState = state;
    }

    public abstract String getEntityName();

    public abstract String getDetails();

    public Coordinates getLocation() {
        return location;
    }

    public final void setLocation(Coordinates c) {
        this.location = c;
    }
}
