package iuto.idirm.cpoa;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;

/**
 * @since JDK 15
 */
public class Command <T> {
    private final String name;
    private String[] aliases;
    private String desc;
    private final T target;
    private Method method;

    public Command(T target, String name) throws NullCommandNameException {
        if (name == null) throw new NullCommandNameException("Command name cannot be null");
        this.name = name;
        this.target = target;
        this.aliases = new String[]{name};
    }
    public Command(T target, String name, String a1) throws NullCommandNameException {
        this(target, name);
        this.addAlias(a1);
    }
    public Command(T target, String name, String a1, String a2) throws NullCommandNameException {
        this(target, name, a1);
        this.addAlias(a2);
    }
    public Command(T target, String name, String a1, String a2, String a3) throws NullCommandNameException {
        this(target, name, a1, a2);
        this.addAlias(a3);
    }
    public Command(T target, String name, String a1, String a2, String a3, String a4) throws NullCommandNameException {
        this(target, name, a1, a2, a3);
        this.addAlias(a4);
    }
    public Command(T target, String name, String... aliases) throws NullCommandNameException {
        this(target, name);
        for (String alias : aliases) {
            this.addAlias(alias);
        }
    }

    @SuppressWarnings("rawtypes")
    public Command addAlias(String alias) {
        if (Arrays.asList(this.aliases).contains(alias)) return this;
        int i = this.aliases.length;
        this.aliases = Arrays.copyOf(this.aliases, this.aliases.length+1);
        this.aliases[i] = alias;
        Arrays.sort(this.aliases, 1, this.aliases.length-1);
        return this;
    }

    @SuppressWarnings("rawtypes")
    public Command exec(Method m) throws MethodClassException {
        Class<?> c = m.getDeclaringClass();
        if (target != null && c.equals(target.getClass()))
            throw new MethodClassException(String.format("Unmatching method source class %s",c.getCanonicalName()));
        this.method = m;
        return this;
    }

    @SuppressWarnings("rawtypes")
    public Command setDesc(String s) {
        this.desc = s;
        return this;
    }

    public boolean run(String[] args) throws InvocationTargetException, IllegalAccessException {
        if (!ifInArray(aliases,args[0])) return false;
        method.invoke(target, (Object) Arrays.copyOfRange(args, 1, args.length));
        return true;
    }

    public String getName() {
        return this.name;
    }

    public Collection<String> getAliases() {
        return Arrays.asList(Arrays.copyOfRange(this.aliases, 1, this.aliases.length));
    }

    public String getDesc() {
        return this.desc;
    }

    private static <E> boolean ifInArray (E[] arr, E item) {
        return Arrays.asList(arr).contains(item);
    }

    public static class MethodClassException extends Exception {
        public MethodClassException(String s) {
            super(s);
        }
    }
    public static class NullCommandNameException extends Exception {
        public NullCommandNameException(String s) {
            super(s);
        }
    }
    public static class CommandComparator implements Comparator<Command<?>> {

        @Override
        public int compare(Command<?> o1, Command<?> o2) {
            return o1.getName().compareTo(o2.getName());
        }
    }
}
