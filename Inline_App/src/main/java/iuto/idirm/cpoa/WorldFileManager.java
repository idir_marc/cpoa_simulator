package iuto.idirm.cpoa;

import java.io.*;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

import static iuto.idirm.cpoa.AnsiCodes.*;

public final class WorldFileManager {

    public static List<WorldFile> saveFiles() {
        File f = new File("saves/");
        f.mkdirs();
        return Stream.of(f.listFiles())
                .filter(file -> !file.isDirectory())
                .filter(File::canRead)
                .filter(File::canWrite)
                .filter(file -> file.getName().contains("."))
                .filter(WorldFileManager::checkSaveExtension)
                .map(WorldFileManager::getSaveFile).toList();
    }

    private static boolean checkSaveExtension(File f) {
        String s = f.getName();
        return s.contains(".") && s.substring(s.lastIndexOf(".")+1).equalsIgnoreCase("cws");
    } // CWS = CPOA World Simulator

    private static WorldFile getSaveFile(File f) {
        try {
            FileInputStream fileIn = new FileInputStream(f);
            ObjectInputStream in = new ObjectInputStream(fileIn);
            WorldFile wf = (WorldFile) in.readObject();
            in.close();
            fileIn.close();
            return wf;
        } catch (ClassNotFoundException | IOException e) {
            return null;
        }
    }

    public static void load(WorldFile wf) throws IOException, ClassNotFoundException {
        FileInputStream fileIn = new FileInputStream(wf.getFile());
        ObjectInputStream in = new ObjectInputStream(fileIn);
        WorldFile fileWorld = (WorldFile) in.readObject();
        if (fileWorld.hashCode() != wf.hashCode()) throw new IOException();
        World.load((World) in.readObject());
        EntityManager.load((EntityManager) in.readObject());
    }

    public static void save(WorldFile wf) throws IOException {
        File f = wf.getFile();
        FileOutputStream fileOut = new FileOutputStream(f);
        ObjectOutputStream out = new ObjectOutputStream(fileOut);
        out.writeObject(wf);
        out.writeObject(World.getInstance());
    }

    public static class WorldFile implements Serializable {
        public final String worldName;
        public final String fileName;
        public final int width;
        public final int height;
        public final String version;

        public WorldFile(String worldName, String fileName, int width, int height) {
            this.worldName = worldName;
            this.fileName = fileName;
            this.width = width;
            this.height = height;
            this.version = Main.version;
        }

        public WorldFile(String worldName, String fileName, World world) {
            this.worldName = worldName;
            this.fileName = fileName;
            if (world != null) {
                this.width = world.getSizeX();
                this.height = world.getSizeY();
            } else {
                this.width = 0;
                this.height = 0;
            }
            this.version = Main.version;
        }

        public File getFile() {
            return new File(String.format("saves/%s.cws",fileName));
        }

        public boolean compatible() {
            return this.version.equalsIgnoreCase(Main.version);
        }

        @Override
        public String toString() {
            if (!compatible()) return String.format("%s[X] %s (%dx%d) | %s.cws%s",
                    ANSI_FOREGROUND_HI_RED, worldName, width, height, fileName, ANSI_RESET);
            return String.format("%s (%dx%d) | %s.cws", worldName, width, height, fileName);
        }

        @Override
        public int hashCode() {
            return Objects.hash(worldName);
        }
    }
}
