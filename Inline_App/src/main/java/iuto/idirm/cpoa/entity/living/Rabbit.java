package iuto.idirm.cpoa.entity.living;

import iuto.idirm.cpoa.World;
import iuto.idirm.cpoa.entity.Entity;
import iuto.idirm.cpoa.entity.food.Food;
import iuto.idirm.cpoa.map.Biome;
import iuto.idirm.cpoa.map.Cardinals;

import java.util.Map;

/**
 * @since JDK 15
 */
public class Rabbit extends LivingEntity {
    public Rabbit(int id) {
        super(id);
        this.growthTime = 5;
        this.weight = 1;
    }

    public static Rabbit make(int id, String[] args) {
        return new Rabbit(id);
    }

    @Override
    protected int[] reproduce(LivingEntity e) {
        return new int[0];
    }

    @Override
    public String getDetails() {
        return String.format("Rabbit entity:\n  ID: %d\n  State: %s\n  Health: %d\n  ",
                this.id, this.age, this.health);
    }

    @Override
    public void tick() {
        if (this.lastTickTurn == World.getInstance().getTurn()) return;
        super.tick();
        this.age.tick();
        if (this.energy <= 0) return;
        this.behaviour();
    }

    @Override
    public void behaviour() {
        this.age.behaviour();
    }

    @Override
    public void move(Cardinals c) {
    }

    @Override
    public void grow() {

    }

    @Override
    public void eat(Food f) {

    }

    @Override
    public void attack(Entity e) {

    }

    @Override
    public void deathDrop() {

    }

    @Override
    public Map<Cardinals, Biome> look() {
        return null;
    }
}
