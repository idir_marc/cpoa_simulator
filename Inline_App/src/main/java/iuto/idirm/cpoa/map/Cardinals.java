package iuto.idirm.cpoa.map;

/**
 * @since JDK 15
 */
public enum Cardinals {
    UP ("UP", -1),
    RIGHT ("RIGHT", 1),
    DOWN ("DOWN", 1),
    LEFT ("LEFT", -1);

    public final String str;
    public final int factor;

    Cardinals(String s, int f) {
        this.str = s;
        this.factor = f;
    }

    @Override
    public String toString() {
        return str;
    }
}
