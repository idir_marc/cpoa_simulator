package iuto.idirm.cpoa.entity.vegetal;

import iuto.idirm.cpoa.World;
import iuto.idirm.cpoa.entity.Entity;
import iuto.idirm.cpoa.entity.EntityManager;
import iuto.idirm.cpoa.entity.food.Carrot;
import iuto.idirm.cpoa.entity.food.Food;
import iuto.idirm.cpoa.entity.state.age.BabyState;
import iuto.idirm.cpoa.map.Biome;
import iuto.idirm.cpoa.map.Cardinals;

import java.util.Map;

/**
 * @since JDK 15
 */
public class CarrotPlant extends VegetalEntity {

    public CarrotPlant(int id) {
        super(id);
        this.age = new BabyState(this);
        this.growthTime = 3;
        this.produceDelay = 5;
    }

    public static CarrotPlant make(int id, String[] args) {
        return new CarrotPlant(id);
    }

    @Override
    public String getDetails() {
        return String.format("Carrot Plant entity:\n  ID: %d\n  State: %s\n  Produce delay: %d\n  Location: %s",
                this.id, this.age, this.produceDelay, this.location);
    }

    @Override
    protected final void produce() {
        this.produceDelay -= 1;
        if (this.produceDelay == 0) {
            this.produceDelay = 5;
            this.seed(new CarrotPlant(EntityManager.getInstance().entityNewID()));
        }
    }

    @Override
    public void move(Cardinals c) {

    }

    @Override
    public void eat(Food f) {

    }

    @Override
    public void attack(Entity e) {

    }

    @Override
    public void deathDrop() {
        int v = this.calcDeathFoodValue() + Carrot.getDefaultFoodValue();
        Carrot c = new Carrot(EntityManager.getInstance().entityNewID(), v);
        c.setLocation(this.location);
        EntityManager.getInstance().addEntity(c);
        World.getInstance().getBiomeAt(this.location).addEntity(c.getId());
    }

    @Override
    public Map<Cardinals, Biome> look() {
        return null;
    }
}
