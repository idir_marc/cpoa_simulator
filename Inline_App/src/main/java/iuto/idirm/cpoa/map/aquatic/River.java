package iuto.idirm.cpoa.map.aquatic;

import iuto.idirm.cpoa.AnsiCodes;

/**
 * @since JDK 15
 */
public class River extends AquaticBiome {

    @Override
    public String toString() {
        return String.format("%sX", AnsiCodes.ANSI_FOREGROUND_CYAN);
    }
}
