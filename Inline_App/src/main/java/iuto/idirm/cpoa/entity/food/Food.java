package iuto.idirm.cpoa.entity.food;

import iuto.idirm.cpoa.entity.Entity;

/**
 * @since JDK 15
 */
public abstract class Food extends Entity {
    protected boolean meat;
    public static final int speed = 0;

    public Food(int id, int e) {
        super(id);
        this.energy = e;
        this.weight = 1;
    }

    @Override
    public final void tick() {
        this.lifeTime++;
        this.energy --;
        super.tick();
    }

    @Override
    public final void behaviour() {}
}
