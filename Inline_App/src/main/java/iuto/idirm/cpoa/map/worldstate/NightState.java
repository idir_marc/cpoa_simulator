package iuto.idirm.cpoa.map.worldstate;

import iuto.idirm.cpoa.World;

/**
 * @since JDK 15
 */
public class NightState extends DayNightState {
    NightState() {
        this.age = 0;
        this.brightness = 0;
        this.duration = 3;
    }

    @Override
    public String typeString() {
        return "Night";
    }

    @Override
    public void endState() {
        this.age = 0;
        World.getInstance().updateState(WorldStates.morning);
    }
}
