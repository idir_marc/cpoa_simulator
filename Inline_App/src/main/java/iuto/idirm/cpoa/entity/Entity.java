package iuto.idirm.cpoa.entity;

import iuto.idirm.cpoa.World;
import iuto.idirm.cpoa.entity.state.age.EntityAgeState;
import iuto.idirm.cpoa.entity.vegetal.VegetalEntity;
import iuto.idirm.cpoa.map.Coordinates;

import java.io.Serializable;
import java.util.Comparator;

/**
 * @since JDK 15
 */
public abstract class Entity implements IEntityActions, Serializable {
    protected final int id;
    protected int lifeTime;
    protected EntityAgeState age;
    protected int energy;
    protected int weight;
    protected int health;
    protected int growthTime;
    protected boolean gender;
    protected Coordinates location;
    protected int speed;
    protected int lastTickTurn = World.getInstance().getTurn();

    protected Entity(int id) {
        this.id = id;
    }

    public final int getId() {
        return this.id;
    }

    public final EntityAgeState getAgeState() {
        return this.age;
    }

    public final int getGrowthSpeed() {
        return this.growthTime;
    }

    public final void switchAgeState(EntityAgeState state) {
        this.age = state;
    }

    public abstract String getDetails();

    public Coordinates getLocation() {
        return location;
    }

    public final void setLocation(Coordinates c) {
        this.location = c;
    }

    public void kill() {
        this.deathDrop();
        World.getInstance().removeEntity(this.location, this.id);
        EntityManager.getInstance().removeEntity(this.id);
    }


    @Override
    public void tick() {
        this.lastTickTurn = World.getInstance().getTurn();
        this.energy -= 1;
        if (this.energy <= 0) this.kill();
    }

    public static class EntityComparator implements Comparator<Entity> {
        @Override
        public int compare(Entity o1, Entity o2) {
            if (o1 == null && o2 != null) return -1;
            if (o2 == null && o1 != null) return 1;
            if (o1 == null) return 0;
            if (o1 instanceof VegetalEntity) {
                if (o2 instanceof VegetalEntity) return 0;
                return 1;
            }
            if (o2 instanceof VegetalEntity) return -1;
            try {
                return Integer.compare(o1.getClass().getField("speed").getInt(null),
                        o2.getClass().getField("speed").getInt(null));
            } catch (IllegalAccessException | NoSuchFieldException e) {
                return 0;
            }
        }
    }
}
