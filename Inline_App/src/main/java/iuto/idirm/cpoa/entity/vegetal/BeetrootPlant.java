package iuto.idirm.cpoa.entity.vegetal;

import iuto.idirm.cpoa.World;
import iuto.idirm.cpoa.entity.Entity;
import iuto.idirm.cpoa.entity.EntityManager;
import iuto.idirm.cpoa.entity.food.Beetroot;
import iuto.idirm.cpoa.entity.food.Carrot;
import iuto.idirm.cpoa.entity.food.Food;
import iuto.idirm.cpoa.entity.state.age.BabyState;
import iuto.idirm.cpoa.map.Biome;
import iuto.idirm.cpoa.map.Cardinals;

import java.util.Map;

/**
 * @since JDK 15
 */
public class BeetrootPlant extends VegetalEntity {

    public BeetrootPlant(int id) {
        super(id);
        this.produceDelay = 4;
        this.growthTime = 3;
        this.age = new BabyState(this);
    }

    public static BeetrootPlant make(int id, String[] args) {
        return new BeetrootPlant(id);
    }

    @Override
    public String getDetails() {
        return String.format("Beetroot Plant entity:\n  ID: %d\n  State: %s\n  Produce delay: %d\n  Location: %s",
                this.id, this.age, this.produceDelay, this.location);
    }

    @Override
    protected final void produce() {
        this.produceDelay -= 1;
        if (this.produceDelay == 0) {
            this.produceDelay = 4;
            this.seed(new BeetrootPlant(EntityManager.getInstance().entityNewID()));
        }
    }

    @Override
    public void move(Cardinals c) {

    }

    @Override
    public void eat(Food f) {

    }

    @Override
    public void attack(Entity e) {

    }

    @Override
    public void deathDrop() {
        int v = this.calcDeathFoodValue() + Beetroot.getDefaultFoodValue();
        Beetroot b = new Beetroot(EntityManager.getInstance().entityNewID(), v);
        b.setLocation(this.location);
        EntityManager.getInstance().addEntity(b);
        World.getInstance().getBiomeAt(this.location).addEntity(b.getId());
    }

    @Override
    public Map<Cardinals, Biome> look() {
        return null;
    }
}
