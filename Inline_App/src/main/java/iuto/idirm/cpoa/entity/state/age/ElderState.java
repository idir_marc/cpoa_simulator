package iuto.idirm.cpoa.entity.state.age;

import iuto.idirm.cpoa.entity.Entity;
import iuto.idirm.cpoa.entity.food.Food;
import iuto.idirm.cpoa.map.Biome;
import iuto.idirm.cpoa.map.Cardinals;

import java.util.Map;

/**
 * @since JDK 15
 */
public class ElderState extends EntityAgeState {
    public ElderState(Entity e) {
        super(e);
    }

    @Override
    public void tick() {
        this.since++;
    }

    @Override
    public void behaviour() {

    }

    @Override
    public int ageLevel() {
        return 3;
    }

    @Override
    public void move(Cardinals c) {

    }

    @Override
    public void grow() {
        if (this.since > this.entity.getGrowthSpeed()) this.entity.kill();
    }

    @Override
    public void eat(Food f) {

    }

    @Override
    public void attack(Entity e) {

    }

    @Override
    public void deathDrop() {

    }

    @Override
    public int deathFoodQuality() {
        return 1;
    }

    @Override
    public Map<Cardinals, Biome> look() {
        return null;
    }

    @Override
    public String toString() {
        return "elder";
    }
}
