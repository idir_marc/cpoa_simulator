package iuto.idirm.cpoa.map.ground;

import iuto.idirm.cpoa.AnsiCodes;

/**
 * @since JDK 15
 */
public class Forest extends GroundBiome {

    @Override
    public String toString() {
        return String.format("%sX", AnsiCodes.ANSI_FOREGROUND_GREEN);
    }
}
