package iuto.idirm.cpoa.map.worldstate;

import iuto.idirm.cpoa.World;

/**
 * @since JDK 15
 */
public class EveningState extends DayNightState{
    EveningState() {
        this.age = 0;
        this.brightness = 1;
        this.duration = 1;
    }

    @Override
    public String typeString() {
        return "Evening";
    }

    @Override
    public void endState() {
        this.age = 0;
        World.getInstance().updateState(WorldStates.night);
    }
}
