package iuto.idirm.cpoa.entity.living;

import iuto.idirm.cpoa.entity.Entity;
import iuto.idirm.cpoa.entity.food.Food;
import iuto.idirm.cpoa.map.Biome;
import iuto.idirm.cpoa.map.Cardinals;

import java.util.Map;

/**
 * @since JDK 15
 */
public class Cat extends LivingEntity {
    public Cat(int id) {
        super(id);
    }

    public static Cat make(int id, String[] args) {
        System.out.println("Cats not implemented yet");
        return null;
    }

    @Override
    protected int[] reproduce(LivingEntity e) {
        return new int[0];
    }

    @Override
    public String getDetails() {
        return null;
    }

    @Override
    public void tick() {

    }

    @Override
    public void behaviour() {

    }

    @Override
    public void move(Cardinals c) {

    }

    @Override
    public void grow() {

    }

    @Override
    public void eat(Food f) {

    }

    @Override
    public void attack(Entity e) {

    }

    @Override
    public void deathDrop() {

    }

    @Override
    public Map<Cardinals, Biome> look() {
        return null;
    }
}
