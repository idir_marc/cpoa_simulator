package iuto.idirm.cpoa.entity.state.age;

import iuto.idirm.cpoa.entity.Entity;
import iuto.idirm.cpoa.entity.IEntityActions;

/**
 * @since JDK 15
 */
public abstract class EntityAgeState implements IEntityActions {
    protected Entity entity;
    protected int since = 0;

    public EntityAgeState(Entity e) {
        this.entity = e;
    }

    public abstract int deathFoodQuality();

    public abstract int ageLevel();
}
