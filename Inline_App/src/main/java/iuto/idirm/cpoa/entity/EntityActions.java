package iuto.idirm.cpoa.entity;

/**
 * @since JDK 15
 */
public enum EntityActions {
    MOVE,
    GROW,
    EAT,
    ATTACK,
    LOOK // or listen, pay attention
}
