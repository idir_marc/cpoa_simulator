package iuto.idirm.cpoa.entity.vegetal;

import iuto.idirm.cpoa.World;
import iuto.idirm.cpoa.entity.Entity;
import iuto.idirm.cpoa.entity.EntityManager;
import iuto.idirm.cpoa.entity.food.Food;
import iuto.idirm.cpoa.entity.state.age.AdultState;
import iuto.idirm.cpoa.map.Coordinates;
import iuto.idirm.cpoa.map.Wind;

/**
 * @since JDK 15
 */
public abstract class VegetalEntity extends Entity {
    protected Food product;
    protected int produceDelay;

    public VegetalEntity(int id) {
        super(id);
    }

    protected final int calcDeathFoodValue() {
        return this.energy * this.age.deathFoodQuality();
    }

    protected abstract void produce();

    protected final void seed(Entity e) {
        if (EntityManager.getInstance().addEntity(e) == 0) return;
        int x = this.getLocation().getX();
        int y = this.getLocation().getY();
        Wind w = World.getInstance().getWind();
        x = Math.min(Math.max(0, (int)(x + w.getPower() * 2 * w.getRightLeftStrength() * w.getRightLeft().factor)),
                World.getInstance().getSizeX()-1);
        y = Math.min(Math.max(0, (int)(y + w.getPower() * 2 * w.getUpDownStrength() * w.getUpDown().factor)),
                World.getInstance().getSizeY()-1);
        Coordinates c = new Coordinates(x, y);
        e.setLocation(c);
        World.getInstance().getBiomeAt(c).addEntity(e.getId());
    }

    @Override
    public void grow() {
        this.lifeTime += 1;
        this.age.grow();
        if (this.age instanceof AdultState) this.produce();
    }

    @Override
    public void tick() {
        this.age.tick();
        this.grow();
    }

    @Override
    public final void behaviour() {}
}
